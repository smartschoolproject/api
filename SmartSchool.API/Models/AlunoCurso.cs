﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartSchool.API.Models
{
    public class AlunoCurso
    {
        public AlunoCurso()
        {

        }
        public AlunoCurso(int id, int alunoId, int cursoId)
        {
            this.Id = id;
            this.AlunoId = alunoId;
            this.CursoId = cursoId;

        }
        public int Id { get; set; }
        public DateTime DataIni { get; set; } = DateTime.Now;
        public DateTime? DataFim { get; set; } = null;
        public int? Nota { get; set; }
        public int AlunoId { get; set; }
        public int CursoId { get; set; }

        public Aluno Aluno { get; set; }
        public Curso Curso { get; set; }
    }
}
