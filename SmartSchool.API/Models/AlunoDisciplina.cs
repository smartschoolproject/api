using System;

namespace SmartSchool.API.Models
{
    public class AlunoDisciplina
    {
        public AlunoDisciplina()
        {

        }
        public AlunoDisciplina(int id, int alunoId, int disciplinaId)
        {
            this.Id = id;
            this.AlunoId = alunoId;
            this.DisciplinaId = disciplinaId;

        }
        public int Id { get; set; }
        public DateTime DataIni { get; set; } = DateTime.Now;
        public DateTime? DataFim { get; set; } = null;
        public int? Nota { get; set; }
        public int AlunoId { get; set; }
        public int DisciplinaId { get; set; }

        public Aluno Aluno { get; set; }
        public Disciplina Disciplina { get; set; }
    }
}