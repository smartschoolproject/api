﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartSchool.API.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Alunos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Matricula = table.Column<int>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Sobrenome = table.Column<string>(nullable: true),
                    Telefone = table.Column<string>(nullable: true),
                    DataNasc = table.Column<DateTime>(nullable: false),
                    DataIni = table.Column<DateTime>(nullable: false),
                    DataFim = table.Column<DateTime>(nullable: true),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alunos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cursos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cursos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Professores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Registro = table.Column<int>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Sobrenome = table.Column<string>(nullable: true),
                    Telefone = table.Column<string>(nullable: true),
                    DataIni = table.Column<DateTime>(nullable: false),
                    DataFim = table.Column<DateTime>(nullable: true),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AlunoCursos",
                columns: table => new
                {
                    AlunoId = table.Column<int>(nullable: false),
                    CursoId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    DataIni = table.Column<DateTime>(nullable: false),
                    DataFim = table.Column<DateTime>(nullable: true),
                    Nota = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlunoCursos", x => new { x.AlunoId, x.CursoId });
                    table.ForeignKey(
                        name: "FK_AlunoCursos_Alunos_AlunoId",
                        column: x => x.AlunoId,
                        principalTable: "Alunos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlunoCursos_Cursos_CursoId",
                        column: x => x.CursoId,
                        principalTable: "Cursos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Disciplinas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true),
                    CargaHoraria = table.Column<int>(nullable: false),
                    ProfessorId = table.Column<int>(nullable: false),
                    PreRequisitoId = table.Column<int>(nullable: true),
                    CursoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Disciplinas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Disciplinas_Cursos_CursoId",
                        column: x => x.CursoId,
                        principalTable: "Cursos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Disciplinas_Disciplinas_PreRequisitoId",
                        column: x => x.PreRequisitoId,
                        principalTable: "Disciplinas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Disciplinas_Professores_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AlunosDisciplinas",
                columns: table => new
                {
                    AlunoId = table.Column<int>(nullable: false),
                    DisciplinaId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    DataIni = table.Column<DateTime>(nullable: false),
                    DataFim = table.Column<DateTime>(nullable: true),
                    Nota = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlunosDisciplinas", x => new { x.AlunoId, x.DisciplinaId });
                    table.ForeignKey(
                        name: "FK_AlunosDisciplinas_Alunos_AlunoId",
                        column: x => x.AlunoId,
                        principalTable: "Alunos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlunosDisciplinas_Disciplinas_DisciplinaId",
                        column: x => x.DisciplinaId,
                        principalTable: "Disciplinas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Alunos",
                columns: new[] { "Id", "Ativo", "DataFim", "DataIni", "DataNasc", "Matricula", "Nome", "Sobrenome", "Telefone" },
                values: new object[,]
                {
                    { 1, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(2946), new DateTime(1988, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "Marta", "Kent", "33225555" },
                    { 2, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(4440), new DateTime(1988, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "Paula", "Isabela", "3354288" },
                    { 3, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(4490), new DateTime(1988, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Laura", "Antonia", "55668899" },
                    { 4, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(4499), new DateTime(1988, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "Luiza", "Maria", "6565659" },
                    { 5, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(4504), new DateTime(1988, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Lucas", "Machado", "565685415" },
                    { 6, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(4512), new DateTime(1988, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, "Pedro", "Alvares", "456454545" },
                    { 7, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(4518), new DateTime(1988, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 7, "Paulo", "José", "9874512" }
                });

            migrationBuilder.InsertData(
                table: "Cursos",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 1, "TEcnologia da Informação" },
                    { 2, "Sistemas de Informação" },
                    { 3, "Ciência da Computação" }
                });

            migrationBuilder.InsertData(
                table: "Professores",
                columns: new[] { "Id", "Ativo", "DataFim", "DataIni", "Nome", "Registro", "Sobrenome", "Telefone" },
                values: new object[,]
                {
                    { 1, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 681, DateTimeKind.Local).AddTicks(2167), "Lauro", 123, "Lauro", null },
                    { 2, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 684, DateTimeKind.Local).AddTicks(2659), "Roberto", 123, "Lauro", null },
                    { 3, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 684, DateTimeKind.Local).AddTicks(2698), "Ronaldo", 123, "Lauro", null },
                    { 4, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 684, DateTimeKind.Local).AddTicks(2701), "Rodrigo", 123, "Lauro", null },
                    { 5, true, null, new DateTime(2021, 1, 6, 19, 43, 59, 684, DateTimeKind.Local).AddTicks(2703), "Alexandre", 123, "Lauro", null }
                });

            migrationBuilder.InsertData(
                table: "Disciplinas",
                columns: new[] { "Id", "CargaHoraria", "CursoId", "Nome", "PreRequisitoId", "ProfessorId" },
                values: new object[,]
                {
                    { 1, 0, 1, "Matemática", null, 1 },
                    { 2, 0, 3, "Física", null, 1 },
                    { 3, 0, 3, "Português", null, 2 },
                    { 4, 0, 1, "Inglês", null, 3 },
                    { 5, 0, 1, "Programação", null, 4 },
                    { 6, 0, 2, "Matemática", null, 4 },
                    { 7, 0, 3, "Física", null, 4 },
                    { 8, 0, 1, "Português", null, 5 },
                    { 9, 0, 2, "Inglês", null, 5 },
                    { 10, 0, 3, "Programação", null, 5 }
                });

            migrationBuilder.InsertData(
                table: "AlunosDisciplinas",
                columns: new[] { "AlunoId", "DisciplinaId", "DataFim", "DataIni", "Id", "Nota" },
                values: new object[,]
                {
                    { 2, 1, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6472), 0, null },
                    { 4, 5, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6489), 0, null },
                    { 2, 5, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6478), 0, null },
                    { 1, 5, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6470), 0, null },
                    { 7, 4, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6504), 0, null },
                    { 6, 4, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6499), 0, null },
                    { 5, 4, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6490), 0, null },
                    { 4, 4, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6487), 0, null },
                    { 1, 4, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6443), 0, null },
                    { 7, 3, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6503), 0, null },
                    { 5, 5, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6492), 0, null },
                    { 6, 3, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6496), 0, null },
                    { 7, 2, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6502), 0, null },
                    { 6, 2, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6494), 0, null },
                    { 3, 2, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6481), 0, null },
                    { 2, 2, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6473), 0, null },
                    { 1, 2, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(5982), 0, null },
                    { 7, 1, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6500), 0, null },
                    { 6, 1, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6493), 0, null },
                    { 4, 1, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6486), 0, null },
                    { 3, 1, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6480), 0, null },
                    { 3, 3, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6483), 0, null },
                    { 7, 5, null, new DateTime(2021, 1, 6, 19, 43, 59, 686, DateTimeKind.Local).AddTicks(6506), 0, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlunoCursos_CursoId",
                table: "AlunoCursos",
                column: "CursoId");

            migrationBuilder.CreateIndex(
                name: "IX_AlunosDisciplinas_DisciplinaId",
                table: "AlunosDisciplinas",
                column: "DisciplinaId");

            migrationBuilder.CreateIndex(
                name: "IX_Disciplinas_CursoId",
                table: "Disciplinas",
                column: "CursoId");

            migrationBuilder.CreateIndex(
                name: "IX_Disciplinas_PreRequisitoId",
                table: "Disciplinas",
                column: "PreRequisitoId");

            migrationBuilder.CreateIndex(
                name: "IX_Disciplinas_ProfessorId",
                table: "Disciplinas",
                column: "ProfessorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AlunoCursos");

            migrationBuilder.DropTable(
                name: "AlunosDisciplinas");

            migrationBuilder.DropTable(
                name: "Alunos");

            migrationBuilder.DropTable(
                name: "Disciplinas");

            migrationBuilder.DropTable(
                name: "Cursos");

            migrationBuilder.DropTable(
                name: "Professores");
        }
    }
}
