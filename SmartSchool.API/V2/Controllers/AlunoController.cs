﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartSchool.API.Data;
using SmartSchool.API.V1.Dtos;
using SmartSchool.API.Models;
using System.Collections.Generic;
using System.Linq;


namespace SmartSchool.API.V2.Controllers
{
    /// <summary>
    /// Versão 2.0 do controlador de aluno
    /// </summary>
    [ApiController]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AlunoController : ControllerBase
    {
        private readonly IRepository _repo;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repo"></param>
        /// <param name="mapper"></param>
        public AlunoController(IRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// Método responsável para retornar todos os meus alunos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var alunos = _repo.GetAllAlunos(true);
                
                return Ok(_mapper.Map<IEnumerable<AlunoDto>>(alunos));
            }
            catch (System.Exception)
            {
                throw;
            }
            
        }
             

        /// <summary>
        /// Método responsável por retornar apenas um aluno por ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var aluno = _repo.GetAllAlunosById(id);
            if (aluno == null)
            {
                return BadRequest("O aluno não foi encontrado");
            }

            var alunoDto = _mapper.Map<AlunoDto>(aluno);
            return Ok(alunoDto);
        }

        //[HttpGet("{ByName}")]
        //public IActionResult GetByName(string nome, string sobrenome)
        //{
        //    var aluno = _context.Alunos.FirstOrDefault(a =>
        //    a.Nome.Contains(nome) && a.Sobrenome.Contains(sobrenome));
        //    if (aluno == null)
        //    {
        //        return BadRequest("O aluno não foi encontrado");
        //    }
        //    return Ok(aluno);
        //}

        /// <summary>
        /// Método responsável por cadastrar Aluno
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Post(AlunoRegistrarDto model)
        {
            var aluno = _mapper.Map<Aluno>(model);

            _repo.Add(aluno);
            if (_repo.SaveChanges())
            {
                return Created($"/api/aluno/{model.Id}", _mapper.Map<AlunoDto>(aluno));
            }
            return BadRequest("Aluno não cadastrado");
        }

        [HttpPut]
        public ActionResult Put(int id, AlunoRegistrarDto model)
        {
            var aluno = _repo.GetAllAlunosById(id);
            if (aluno == null) return BadRequest("Aluno não encontrado");

            _mapper.Map(model, aluno);

            _repo.Update(aluno);
            if (_repo.SaveChanges())
            {
                return Created($"/api/aluno/{model.Id}", _mapper.Map<AlunoDto>(aluno));
            }
            return BadRequest("Aluno não atualizado");
        }
              

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            var aluno = _repo.GetAllAlunosById(id);
            if (aluno == null) return BadRequest("Aluno não encontrado");

            _repo.Delete(aluno);
            if (_repo.SaveChanges())
            {
                return Ok("Aluno deletado");
            }
            return BadRequest("Aluno não deletado");
        }
    }
}
