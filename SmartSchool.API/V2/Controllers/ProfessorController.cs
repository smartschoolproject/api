﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartSchool.API.Data;
using SmartSchool.API.V1.Dtos;
using SmartSchool.API.Models;
using System.Collections.Generic;
using System.Linq;

namespace SmartSchool.API.V2.Controllers
{
    /// <summary>
    /// Versão 2.0 do controlador de professor
    /// </summary>
    [ApiController]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ProfessorController : ControllerBase
    {
        private readonly IRepository _repo;
        private readonly IMapper _mapper;

        public ProfessorController(IRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var professores = _repo.GetAllProfessores(true);

                return Ok(_mapper.Map<IEnumerable<ProfessorDto>>(professores));
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var professor = _repo.GetAllProfessoresById(id);
            if (professor == null)
            {
                return BadRequest("O Professor não foi encontrado");
            }

            var professorDto = _mapper.Map<ProfessorDto>(professor);
            return Ok(professorDto);
        }

        //[HttpGet("{ByName}")]
        //public IActionResult GetByName(string nome)
        //{
        //    var Professor = _repo.GetAllProfessoresById(id);
        //    if (Professor == null)
        //    {
        //        return BadRequest("O Professor não foi encontrado");
        //    }
        //    return Ok(Professor);
        //}

        [HttpPost]
        public ActionResult Post(ProfessorRegistrarDto model)
        {
            var professor = _mapper.Map<Professor>(model);

            _repo.Add(professor);

            if (_repo.SaveChanges())
            {
                return Created($"/api/professor/{model.Id}", _mapper.Map<ProfessorDto>(professor));
            }
            return BadRequest("Professor não cadastrado");
        }

        [HttpPut]
        public ActionResult Put(int id, ProfessorRegistrarDto model)
        {
            var professor = _repo.GetAllAlunosById(id);
            if (professor == null) return BadRequest("Professor não encontrado");

            _mapper.Map(model, professor);

            _repo.Update(professor);
            if (_repo.SaveChanges())
            {
                return Created($"/api/professor/{model.Id}", _mapper.Map<ProfessorDto>(professor));
            }
            return BadRequest("Professor não atualizado");
        }

        [HttpPatch("{id}")]
        public ActionResult Patch(int id, ProfessorRegistrarDto model)
        {
            var professor = _repo.GetAllAlunosById(id);
            if (professor == null) return BadRequest("Professor não encontrado");

            _mapper.Map(model, professor);
            _repo.Update(professor);
            if (_repo.SaveChanges())
            {
                return Created($"/api/professor/{model.Id}", _mapper.Map<ProfessorDto>(professor));
            }
            return BadRequest("Professor não atualizado");
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            var propf = _repo.GetAllProfessoresById(id);
            if (propf == null) return BadRequest("Professor não encontrado");

            _repo.Update(propf);
            if (_repo.SaveChanges())
            {
                return Ok("Professor deletado");
            }
            return BadRequest("Professor não deletado");
        }
    }
}
