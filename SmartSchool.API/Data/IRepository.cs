﻿using SmartSchool.API.Helpers;
using SmartSchool.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartSchool.API.Data
{
  public interface IRepository
  {
    void Add<T>(T entity) where T : class;
    void Update<T>(T entity) where T : class;
    void Delete<T>(T entity) where T : class;
    bool SaveChanges();

    Task<PageList<Aluno>> GetAllAlunosAsync(PageParams pageParams, bool incluirProfessor = false);
    Aluno[] GetAllAlunos(bool incluirProfessor = false);
    Aluno[] GetAllAlunosByDisciplinaId(int disciplinaId, bool incluirProfessor = false);
    Aluno GetAllAlunosById(int alunoId, bool incluirProfessor = false);

    Professor[] GetAllProfessores(bool incluirAlunos = false);
    Professor[] GetAllProfessoresByDisciplinaId(int disciplinaId, bool incluirAlunos = false);
    Professor GetAllProfessoresById(int professorId, bool incluirAlunos = false);
    Professor[] GetProfessoresByAlunoId(int alunoId, bool incluirAlunos = false);

  }
}
